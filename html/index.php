<?php
$rd = "../";
require_once $rd . "php/classes/include.php";
require_once $rd . "php/classes/db.php";
if (!$user->isLoggedIn()) {
    header("Location: /login.php");
    die("Please login");
}
$title = $language['title_dashboard'];
require_once $rd . "php/classes/header.php";


echo '
<div class="jumbotron">
    <div class="container">
        <h1 class="display-3">' . $language['dashboard_hello'] . $user->getDisplayName() . '!</h1>
        <p>' . $language['dashboard_hello_sub'] . '</p>
        <p><a class="btn btn-primary btn-lg" href="#" role="button">' . $language['dashboard_hello_learn_more'] . '</a></p>
    </div>
</div>

<div class="container">
    <!-- Example row of columns -->
    <div class="row">
        <div class="col-md-4">
            <h2>' . $language['dashboard_channels'] . '</h2>
            <p>' . $language['dashboard_channels_sub'] . '</p>
            <p><a class="btn btn-secondary" href="/channels.php" role="button">' . $language['continue'] . '</a></p>
        </div>
         <div class="col-md-4">
            <h2>' . $language['dashboard_client'] . '</h2>
            <p>' . $language['dashboard_client_sub'] . '</p>
            <p><a class="btn btn-secondary" href="/clients.php" role="button">' . $language['continue'] . '</a></p>
        </div>
    </div>

    <hr>

</div>';

require_once $rd . "php/classes/footer.php";