<?php
$rd = "../";
require_once $rd . "php/classes/include.php";
require_once $rd . "php/classes/db.php";
if (!$user->isLoggedIn()) {
    header("Location: /login.php");
    die("Please login");
}
$title = $language['title_channels'];
$offcanavas = true;
require_once $rd . "php/classes/header.php";

require_once $rd . "php/classes/ts.php";

//server info
$serverinfo = $tsAdmin->serverInfo()['data'];
echo '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
        <img class="mr-3" src="data:image/png;base64,' . $tsAdmin->getIconByID($serverinfo['virtualserver_icon_id'])['data'] . '" alt="" width="48" height="48">
        <div class="lh-100">
          <h6 class="mb-0 text-white lh-100">' . $serverinfo['virtualserver_name'] . '</h6>
          <small>' . $serverinfo['virtualserver_welcomemessage'] . '</small>
        </div>
      </div>';

echo '<div class="my-3 p-3 bg-white rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Suggestions</h6>';
foreach ($tsAdmin->channelList("-topic -flags -voice -limits")['data'] as $channel) {
    if ($channel['channel_flag_password'] == 1) {
        //password => yellow
        $color = "holder.js/32x32?theme=thumb&bg=FFFF33&fg=FFFF33&size=1";
        $img_title = $language['channel_status_password_protected'];
    } else if ($channel['channel_needed_talk_power'] > 0) {
        $color = "holder.js/32x32?theme=thumb&bg=00B200&fg=00B200&size=1";
        $img_title = $language['channel_status_no_talk_power'];
    } else if ($channel['channel_maxclients'] != -1 && $channel['channel_maxclients'] - $channel['total_clients'] <= 0) {
        $color = "holder.js/32x32?theme=thumb&bg=CD0000&fg=CD0000&size=1";
        $img_title = $language['channel_status_ful'];
    } else {
        $color = 'holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1';
        $img_title = "";
    }
    echo '<div class="media text-muted pt-3">
          <img data-src="' . $color . '" alt="" title="' . $img_title . '" class="mr-2 rounded">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">' . $channel['channel_name'] . '</strong>
              <a href="/channel_edit.php?id=' . $channel['cid'] . '">' . $language['channel_action_edit'] . '</a>
            </div>
            <span class="d-block">' . $channel['channel_topic'] . '</span>
          </div>
        </div>';
}
echo '</div>';


require_once $rd . "php/classes/footer.php";