<?php
$rd = "../";
require_once $rd . "php/classes/include.php";
require_once $rd . "php/classes/db.php";
if (!$user->isLoggedIn()) {
    header("Location: /login.php");
    die("Please login");
}
$title = $language['title_clients_and_channels'];
$offcanavas = true;
require_once $rd . "php/classes/header.php";

require_once $rd . "php/classes/ts.php";


//check if posted
if (isset($_POST['kick'])) {
    kick_client();
}
if (isset($_GET['kicked'])) {
    //show alert
    echo '<div class="alert alert-success" role="alert">
    ' . $language['client_kicked_successfully'] . '
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
}
//check if posted
if (isset($_POST['poke'])) {
    poke_client();
}
if (isset($_GET['poked'])) {
    //show alert
    echo '<div class="alert alert-success" role="alert">
    ' . $language['client_poked_successfully'] . '
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
}

//server info
$serverinfo = $tsAdmin->serverInfo()['data'];
echo '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
        <img class="mr-3" src="data:image/png;base64,' . $tsAdmin->getIconByID($serverinfo['virtualserver_icon_id'])['data'] . '" alt="" width="48" height="48">
        <div class="lh-100">
          <h6 class="mb-0 text-white lh-100">' . $serverinfo['virtualserver_name'] . '</h6>
          <small>' . $serverinfo['virtualserver_welcomemessage'] . '</small>
        </div>
      </div>';

echo '<div class="my-3 p-3 bg-white rounded box-shadow">
        <h6 class="border-bottom border-gray pb-2 mb-0">Suggestions</h6>';
$sgroups_raw = $tsAdmin->serverGroupList()['data'];
$sgroups = array();
foreach ($sgroups_raw as $group) {
    if ($group['type'] != 1)
        continue;
    $sgroups[$group['sgid']] = $group;
}
foreach ($tsAdmin->channelList("-topic -flags -voice -limits")['data'] as $channel) {
    if ($channel['channel_flag_password'] == 1) {
        //password => yellow
        $color = "holder.js/32x32?theme=thumb&bg=FFFF33&fg=FFFF33&size=1";
        $img_title = $language['channel_status_password_protected'];
    } else if ($channel['channel_needed_talk_power'] > 0) {
        $color = "holder.js/32x32?theme=thumb&bg=00B200&fg=00B200&size=1";
        $img_title = $language['channel_status_no_talk_power'];
    } else if ($channel['channel_maxclients'] != -1 && $channel['channel_maxclients'] - $channel['total_clients'] <= 0) {
        $color = "holder.js/32x32?theme=thumb&bg=CD0000&fg=CD0000&size=1";
        $img_title = $language['channel_status_ful'];
    } else {
        $color = 'holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1';
        $img_title = "";
    }
    echo '<div class="media text-muted pt-3">
          <img data-src="' . $color . '" alt="" title="' . $img_title . '" class="mr-2 rounded">
          <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="d-flex justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">' . $channel['channel_name'] . '</strong>
            </div>
            <span class="d-block">' . $channel['channel_topic'] . '</span>
          </div>
        </div>';

    //clients
    $clients = $tsAdmin->channelClientList($channel['cid'], "-uid -away -voice -groups")['data'];
    foreach ($clients as $client) {

        //query clients....ignore
        if ($client['client_type'] != 0) {
            continue;
        }

        if ($client['client_output_muted'] == 1 || $client['client_output_hardware'] == 0 || $client['client_away'] == 1) {
            //deaf or no speakers or away
            $color = "holder.js/32x32?theme=thumb&bg=CD0000&fg=CD0000&size=1";
            $img_title = $language['client_channel_status_deaf'];

        } else if ($client['client_input_muted'] == 1 || $client['client_input_hardware'] == 0) {
            //muted or no mic
            $color = "holder.js/32x32?theme=thumb&bg=FFA500&fg=FFA500&size=1";
            $img_title = $language['client_channel_status_muted'];

        } else {
            $color = 'holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1';
            $img_title = "";
        }

        //ToDo: correct group order

        $name = "";
        $name_print = false;
        foreach (explode(",", $client['client_servergroups']) as $g) {
            if ($sgroups[$g]['namemode'] == 1) {
                //before
                $name .= "[" . $sgroups[$g]['name'] . "] ";
            } else
                if ($sgroups[$g]['namemode'] == 2) {
                    //after
                    //print name if name was not printed before
                    if (!$name_print) {
                        $name .= $client['client_nickname'];
                        $name_print = true;
                    }
                    $name .= " [" . $sgroups[$g]['name'] . "]";
                }
        }
        if (!$name_print) {
            $name .= $client['client_nickname'];
            $name_print = true;
        }
        echo '<div class="media text-muted pt-3" style="margin-left: 60px">
        <img data-src="' . $color . '" alt="" title="' . $img_title . '" class="mr-2 rounded">
            <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
            <div class="justify-content-between align-items-center w-100">
              <strong class="text-gray-dark">' . $name . '</strong>
              <div class="float-right">
                <a data-toggle="modal" data-target="#kick" data-clid="' . $client['clid'] . '" href="#">' . $language['client_action_kick'] . '</a>
                <a style="margin-left: 5px" data-toggle="modal" data-target="#poke" data-clid="' . $client['clid'] . '" href="#">' . $language['client_action_poke'] . '</a>
                <a style="margin-left: 5px" href="/client.php?clid=' . $client['clid'] . '">' . $language['client_action_info'] . '</a>
              </div>
            </div>
            <span class="d-block">' . $client['client_unique_identifier'] . '</span>
          </div>
        </div>';
    }
}
//modals
echo '<div class="modal fade" id="kick" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form method="post">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">' . $language['client_channel_modal_kick_title'] . '</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
            <input id="modal_kick_clid" type="hidden" name="clid">
              <div class="form-group">
                  <label class="control-label">Art</label>
                  <div>
                     <select autocomplete="off" id="mode" class="form-control" name="mode" required>
                        <option disabled hidden selected="selected" value="none">' . $language['dropdown_please_select'] . '</option>
                        <option value="server">' . $language['client_channel_modal_kick_type_server'] . '</option>
                        <option value="channel">' . $language['client_channel_modal_kick_type_channel'] . '</option>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label">' . $language['client_channel_modal_form_reason_label'] . '</label>
                  <div>
                     <textarea class="form-control input-lg" name="reason"></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-outline-danger" data-dismiss="modal">' . $language['cancel'] . '</button>
               <button name="kick" type="submit" class="btn btn-success">' . $language['client_channel_modal_button_kick'] . '</button>
            </div>
         </form>
      </div>
   </div>
</div>
<script>
$("#kick").on("show.bs.modal", function (event) {
  $("#modal_kick_clid").val( $(event.relatedTarget).data("clid"))
})
</script>';
echo '</div>';

//modals
echo '<div class="modal fade" id="poke" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form method="post">
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">' . $language['client_channel_modal_poke_title'] . '</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
            <input id="modal_poke_clid" type="hidden" name="clid">
               <div class="form-group">
                  <label class="control-label">' . $language['client_channel_modal_form_message'] . '</label>
                  <div>
                     <textarea class="form-control input-lg" name="message" required></textarea>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-outline-danger" data-dismiss="modal">' . $language['cancel'] . '</button>
               <button name="poke" type="submit" class="btn btn-success">' . $language['client_channel_modal_button_poke'] . '</button>
            </div>
         </form>
      </div>
   </div>
</div>
<script>
$("#poke").on("show.bs.modal", function (event) {
  $("#modal_poke_clid").val( $(event.relatedTarget).data("clid"))
})
</script>';
echo '</div>';


require_once $rd . "php/classes/footer.php";

function kick_client()
{
    //add token
    global $_POST, $tsAdmin;
    $fields = array("clid", "mode", "reason");
    foreach ($fields as $field) {
        if (!isset($_POST[$field]))
            return;
    }
    if ($_POST['mode'] != "server" && $_POST['mode'] != "channel")
        return;
    if (strlen($_POST['reason']) > 0) {
        $tsAdmin->clientKick(htmlspecialchars($_POST['clid'], ENT_QUOTES), $_POST['mode'], htmlspecialchars($_POST['reason'], ENT_QUOTES));
    } else {
        $tsAdmin->clientKick(htmlspecialchars($_POST['clid'], ENT_QUOTES), $_POST['mode']);
    }
    header("Location: /channel-browser.php?kicked=");
    die();
}

function poke_client()
{
    //add token
    global $_POST, $tsAdmin;
    $fields = array("clid", "message");
    foreach ($fields as $field) {
        if (!isset($_POST[$field]))
            return;
    }
    $tsAdmin->clientPoke(htmlspecialchars($_POST['clid'], ENT_QUOTES), htmlspecialchars($_POST['message'], ENT_QUOTES));
    header("Location: /channel-browser.php?poked=");
    die();
}