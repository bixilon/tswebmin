<?php
$rd = "../";
require_once $rd . "php/classes/include.php";
require_once $rd . "php/classes/db.php";
if (!$user->isLoggedIn()) {
    header("Location: /login.php");
    die("Please login");
}
$title = $language['title_tokens'];
$offcanavas = true;
require_once $rd . "php/classes/header.php";

require_once $rd . "php/classes/ts.php";

if (isset($_GET['delete'])) {
    $token = htmlspecialchars($_GET['delete'], ENT_QUOTES);
    $tsAdmin->tokenDelete($token);
    header("Location: /tokens.php");
    die();
}

if (isset($_GET['add'])) {
    add_token();
}

$tokens = $tsAdmin->tokenList()['data'];
if (count($tokens) > 0) {
    $js_array_sgroups = "";
    //get all server and channel groups
    $sgroups_raw = $tsAdmin->serverGroupList()['data'];
    $sgroups = array();
    foreach ($sgroups_raw as $group) {
        if ($group['type'] != 1)
            continue;
        $sgroups[$group['sgid']] = $group;
        $js_array_sgroups .= "{id: " . $group['sgid'] . ", name:\"" . $group['name'] . "\"},";
    }
    unset($sgroups_raw);
    $js_array_cgroups = "";
    $cgroups_raw = $tsAdmin->channelGroupList()['data'];
    $cgroups = array();
    foreach ($cgroups_raw as $group) {
        if ($group['type'] != 1)
            continue;
        $cgroups[$group['cgid']] = $group;
        $js_array_cgroups .= "{id: " . $group['cgid'] . ", name:\"" . $group['name'] . "\"},";

    }
    unset($cgroups_raw);

    $channels_raw = $tsAdmin->channelList()['data'];
    $channels = array();
    $js_array_channels = "";
    foreach ($channels_raw as $channel) {
        $channels[$channel['cid']] = $channel;
        $js_array_channels .= "{id: " . $channel['cid'] . ", name:\"" . $channel['channel_name'] . "\"},";
    }
    unset($channels_raw);


    echo '<button type="button" class="btn btn-outline-success float-right" style="margin-bottom: 10px" data-toggle="modal" data-target="#add">' . $language['clients_token_add'] . '</button>

<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
      <div class="modal-content">
         <form>
            <div class="modal-header">
               <h5 class="modal-title" id="exampleModalLabel">' . $language['clients_token_add_modal_title'] . '</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="form-group">
                  <label class="control-label">Art</label>
                  <div>
                     <select autocomplete="off" id="type" class="form-control" name="type" onchange="typechange();" required>
                        <option disabled hidden selected="selected" value="none">' . $language['dropdown_please_select'] . '</option>
                        <option value="0">' . $language['clients_token_add_modal_form_servergroup'] . '</option>
                        <option value="1">' . $language['clients_token_add_modal_form_channelgroup'] . '</option>
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label">' . $language['clients_token_add_modal_form_placeholder_group'] . '</label>
                     <select id="group" disabled class="form-control" name="group" required>
                     </select>
                  <div> 
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label">' . $language['clients_token_add_modal_form_placeholder_channel'] . '</label>
                  <div>
                     <select id="channel" disabled class="form-control" name="channel">
                     </select>
                  </div>
               </div>
               <div class="form-group">
                  <label class="control-label">' . $language['clients_token_add_modal_form_placeholder_description'] . '</label>
                  <div>
                     <textarea class="form-control input-lg" name="description"></textarea>
                  </div>
               </div>
            </div>
            <script>
    function typechange() {
    var type = document.getElementById("type");
    var group = document.getElementById("group");
    var channel = document.getElementById("channel");
    var child = group.lastElementChild;
    while (child) {
        group.removeChild(child);
        child = group.lastElementChild;
    }
    child = channel.lastElementChild;
    while (child) {
        channel.removeChild(child);
        child = channel.lastElementChild;
    }
    if(type.value == "none")
        return;
    if (type.value == 0) {
        //server group
        channel.disabled = true;
        channel.required = false;
        group.disabled = false;
        var sgroups = [' . $js_array_sgroups . ']

        sgroups.forEach(function(item) {
            var opt = document.createElement("option");
            opt.value = item["id"];
            opt.innerText = item["name"];
            group.appendChild(opt);
        });
    } else if (type.value == 1) {
        channel.disabled = false;
        channel.required = true;
        group.disabled = false;
        var cgroups = [' . $js_array_cgroups . ']
        cgroups.forEach(function(item) {
            var opt = document.createElement("option");
            opt.value = item["id"];
            opt.innerText = item["name"];
            group.appendChild(opt);
        });

        var channels = [' . $js_array_channels . ']
        channels.forEach(function(item) {
            var opt = document.createElement("option");
            opt.value = item["id"];
            opt.innerText = item["name"];
            channel.appendChild(opt);
        });
    }
}
            </script>
            <div class="modal-footer">
               <button type="button" class="btn btn-outline-danger" data-dismiss="modal">' . $language['clients_token_add_modal_close'] . '</button>
               <button name="add" type="submit" class="btn btn-success">' . $language['clients_token_add_modal_action'] . '</button>
            </div>
         </form>
      </div>
   </div>
</div>
<div class="table-responsive"><table class="table table-striped table-bordered">';
    echo '<thead><tr><th> ' . $language['clients_token_table_token'] . ' </th><th> ' . $language['clients_token_table_type'] . ' </th><th> ' . $language['clients_token_table_group'] . ' </th><th> ' . $language['clients_token_table_channel'] . ' </th><th> ' . $language['clients_token_table_created'] . ' </th><th>' . $language['clients_token_table_description'] . '</th><th>' . $language['clients_token_table_action'] . '</th></tr></thead><tbody>';
    foreach ($tokens as $token) {
        echo "<tr>";
        echo '<td>' . $token['token'] . '</td>';
        if ($token['token_type'] == 0) {
            echo '<td>' . $language['clients_token_server_group'] . '</td>';
            echo '<td>' . $sgroups[$token['token_id1']]['name'] . '</td>';
            echo '<td>- - -</td>';
        } else {
            echo '<td>' . $language['clients_token_channel_group'] . '</td>';
            echo '<td>' . $cgroups[$token['token_id1']]['name'] . '</td>';
            echo '<td>' . $channels[$token['token_id2']]['channel_name'] . '</td>';
        }
        echo '<td>' . dateMe($token['token_created']) . '</td>';
        echo '<td>' . $token['token_description'] . '</td>';
        echo '<td>
        <form action="/tokens.php" class="form-inline my-2 my-lg-0">
          <input name="delete" type="hidden" value="' . $token['token'] . '">
          <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">' . $language['clients_token_table_delete'] . '</button>
        </form></td>';
        echo "</tr>";
    }
    echo '</tbody></table></div>';
} else {
    echo $language['clients_token_no_tokens'];
}

function dateMe($unix)
{
    return date("j.n. y - G:i s", $unix);
}

function add_token()
{
    //add token
    global $_GET, $tsAdmin, $language;
    $fields = array("type", "group", "description");
    foreach ($fields as $field) {
        if (!isset($_GET[$field]))
            return;
    }
    if ($_GET['type'] == 0) {
        //server group
        $token = $tsAdmin->tokenAdd(0, htmlspecialchars($_GET['group'], ENT_QUOTES), 0, htmlspecialchars($_GET['description'], ENT_QUOTES))['data']['token'];
        //okay
    } else {
        if (!isset($_GET['channel']))
            return;
        $token = $tsAdmin->tokenAdd(1, htmlspecialchars($_GET['group'], ENT_QUOTES), htmlspecialchars($_GET['channel'], ENT_QUOTES), htmlspecialchars($_GET['description'], ENT_QUOTES))['data']['token'];
    }
    echo '<div class="alert alert-success m-2" role="alert">
    ' . str_replace('%0', $token, $language['clients_token_add_successfully']) . '
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>';
}

require_once $rd . "php/classes/footer.php";