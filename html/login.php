<?php
$rd = "../";
require_once $rd . "php/classes/include.php";
require_once $rd . "php/classes/db.php";

if ($user->isLoggedIn()) {
    header("Location: /");
    die("You are already logged in!");
}

$title = "Login";
$css_login = true;
$custom = true;
require_once $rd . "php/classes/header.php";


if (isset($_POST['submit'])) {
    $error = login();
    if (!isset($error)) {
        header("Location: /");
        die("You are logged in!");
    }
}

if (!isset($error))
    $error = "";
echo '
<body>
<form class="form-signin" method="post" action="/login.php">
    <div class="text-center mb-4">
        <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72"
             height="72">
        <h1 class="h3 mb-3 font-weight-normal">' . $language['login_header'] . '</h1>
        <p class="login-error" id="error">' . $error . '</p>
    </div>

    <div class="form-label-group">
        <input type="text" name="username" id="inputUsername" class="form-control" placeholder="' . $language['placeholder_username'] . '" required autofocus>
        <label for="inputUsername">' . $language['placeholder_username'] . '</label>
    </div>

    <div class="form-label-group">
        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="' . $language['placeholder_password'] . '" required>
        <label for="inputPassword">' . $language['placeholder_password'] . '</label>
    </div>
    <div class="form-label-group">
        <input type="number" name="2fa" id="input2FA" class="form-control" placeholder="' . $language['placeholder_2fa'] . '" min="0" max="999999" required>
        <label for="input2FA">' . $language['placeholder_2fa'] . '</label>
    </div>

    <div class="checkbox mb-3">
        <label>
            <input type="checkbox" name="remember" value="true"> ' . $language['login_remember_me'] . '
        </label>
    </div>
    <button class="btn btn-lg btn-primary btn-block" name="submit" type="submit">' . $language['login_sign_in'] . '</button>
    <p class="mt-5 mb-3 text-muted text-center">' . $language['copyright'] . '</p>
</form>
</body>
</html>';

//returns error message
function login()
{
    global $_POST, $db, $language, $debug, $rd;
    $fields = array("username", "password", "2fa");
    foreach ($fields as $field) {
        if (!isset($_POST[$field]) || strlen($_POST[$field]) == 0)
            return $language['login_error_field_not_set'];
    }
    $remember = isset($_POST['remember']);

    $res = $db->prepare("SELECT `password`,`token`,`id` FROM `users` WHERE `active` = 1 AND `username` = ?;");
    $res->bindParam(1, $_POST['username']);
    $res->execute();

    //check if user exists
    if ($res->rowCount() == 0)
        return $language['login_wrong'];

    //get data
    $res = $res->fetch(PDO::FETCH_ASSOC);

    //check token
    if (strlen($res['token']) > 5 && !$debug) {
        include_once($rd . "php/lib/GoogleAuthenticator.php");
        $auth = new GoogleAuthenticator();
        if (!$auth->checkCode($res['token'], $_POST['2fa']))
            return $language['login_wrong'];
    }

    //check password
    if (!password_verify($_POST['password'], $res['password']))
        return $language['login_wrong'];

    //everything okay

    //generate session
    $sessionkey = generateRandomString(128);

    $db->exec("INSERT INTO `sessions` (`key`, `user`, `valid`, `time`, `os`, `browser`, `remember_me`) VALUES ('" . $sessionkey . "', '" . $res['id'] . "', '1', '" . time() . "', '" . getOS() . "', '" . getBrowser() . "', '" . boolToInt($remember) . "');");

    if ($remember) {
        //expires not so fast
        setcookie("sessionkey", $sessionkey, time() + 2629746); //1 month
        return null;
    }
    setcookie("sessionkey", $sessionkey); //until end of session

    return null;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getOS()
{

    global $user_agent;

    $os_platform = "Unknown OS Platform";

    $os_array = array(
        '/windows nt 10/i' => 'Windows 10',
        '/windows nt 6.3/i' => 'Windows 8.1',
        '/windows nt 6.2/i' => 'Windows 8',
        '/windows nt 6.1/i' => 'Windows 7',
        '/windows nt 6.0/i' => 'Windows Vista',
        '/windows nt 5.2/i' => 'Windows Server 2003/XP x64',
        '/windows nt 5.1/i' => 'Windows XP',
        '/windows xp/i' => 'Windows XP',
        '/windows nt 5.0/i' => 'Windows 2000',
        '/windows me/i' => 'Windows ME',
        '/win98/i' => 'Windows 98',
        '/win95/i' => 'Windows 95',
        '/win16/i' => 'Windows 3.11',
        '/macintosh|mac os x/i' => 'Mac OS X',
        '/mac_powerpc/i' => 'Mac OS 9',
        '/linux/i' => 'Linux',
        '/ubuntu/i' => 'Ubuntu',
        '/iphone/i' => 'iPhone',
        '/ipod/i' => 'iPod',
        '/ipad/i' => 'iPad',
        '/android/i' => 'Android',
        '/blackberry/i' => 'BlackBerry',
        '/webos/i' => 'Mobile'
    );

    foreach ($os_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $os_platform = $value;

    return $os_platform;
}

function getBrowser()
{

    global $user_agent;

    $browser = "Unknown Browser";

    $browser_array = array(
        '/msie/i' => 'Internet Explorer',
        '/firefox/i' => 'Firefox',
        '/safari/i' => 'Safari',
        '/chrome/i' => 'Chrome',
        '/edge/i' => 'Edge',
        '/opera/i' => 'Opera',
        '/netscape/i' => 'Netscape',
        '/maxthon/i' => 'Maxthon',
        '/konqueror/i' => 'Konqueror',
        '/mobile/i' => 'Handheld Browser'
    );

    foreach ($browser_array as $regex => $value)
        if (preg_match($regex, $user_agent))
            $browser = $value;

    return $browser;
}

function boolToInt($bool)
{
    if ($bool)
        return 1;
    return 0;
}