<?php


//MySQL
$db_name = "database"; //name of mysql data base
$db_host = "127.0.0.1"; //mysql hostname
$db_user = "root"; //mysql user
$db_pw = "secret22"; //mysql password

//E-mail
$smtp_host = "127.0.0.1";
$smtp_port = 143;
$smtp_username = "test@example.com";
$smtp_pw = "secret22";

//TeamSpeak
$ts_host = "127.0.0.1";
$ts_queryport = 10011;
$ts_port = 9987;
$ts_username = "serveradmin";
$ts_password = "12345678";

//E-mail setting
$email_sender = "Example";

//General
$name = "TSWebmin Demo";
$debug = false;

//must be a file name (without ending) in php/lang/
$lang = "de_DE";