<?php
try {
    $db = new PDO("mysql:host=" . $db_host . ";charset=utf8mb4;dbname=" . $db_name, $db_user, $db_pw);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    $db->exec("USE " . $db_name . ";");
} catch (PDOException $e) {
    log("Critical Error: MySQL Connection error: " . $e->getMessage());
    die('Sorry, but there was an error! Please contact the webmaster. Maybe maintenance?');
}