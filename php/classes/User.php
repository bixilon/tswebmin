<?php

class User
{
    function isLoggedIn()
    {
        global $user_is_logged_in, $db;
        if (isset($user_is_logged_in))
            return ($user_is_logged_in);
        $key = getSessionKey();
        if (!isset($key))
            return false;

        $res = $db->prepare("SELECT * FROM `sessions` WHERE `key` = ? AND `valid` = 1;");
        $res->bindParam(1, $key);
        $res->execute();
        if ($res->rowCount() != 1) {
            $this->logout();
            return false;
        }
        $res = $res->fetch(PDO::FETCH_ASSOC);

        $userres = $db->query("SELECT `username`, `displayname`,`group` FROM `users` WHERE `id` = " . $res['user'] . " AND `active` = 1;");
        if ($userres->rowCount() != 1) {
            $this->logout();
            return false;
        }

        if ($res['remember_me']) {
            if (time() - $res['time'] > 2629746) {
                $this->logout();
                return false;
            }
        } else {
            if (time() - $res['time'] > 86400) {
                $this->logout();
                return false;
            }
        }

        $user_is_logged_in = true;
        //set default values
        global $user_name;
        //check if already set
        if (isset($user_name))
            return true;
        //not set. setting "most wanted" ones

        global $user_id, $user_name, $user_dp_name, $user_group;
        $userres = $userres->fetch(PDO::FETCH_ASSOC);

        $user_id = $res['user'];
        $user_name = $userres['username'];
        $user_dp_name = $userres['displayname'];
        $user_group = $userres['group'];


        return true;
    }

    function logout()
    {
        global $db;
        $res = $db->prepare("DELETE FROM `sessions` WHERE `key` = ?;");
        $getSessionKey = getSessionKey();
        $res->bindParam(1, $getSessionKey);
        $res->execute();
        setcookie("key", "", 0);
        return false;
    }

    function getUserName()
    {
        global $user_name, $db;
        if (isset($user_name))
            return $user_name;

        $res = $db->query("SELECT `username` FROM `users` WHERE `id` = '" . $this->getUserID() . "';");
        if ($res->rowCount() == 1) {
            $user_name = $res->fetch(PDO::FETCH_ASSOC)['username'];
            return $user_name;
        }
        return "no_username";
    }

    function getUserID()
    {
        global $user_id, $db;
        if (isset($user_id))
            return $user_id;

        $res = $db->prepare("SELECT `user` FROM `sessions` WHERE `key` = ?;");
        $getSessionKey = getSessionKey();
        $res->bindParam(1, $getSessionKey);
        $res->execute();
        if ($res->rowCount() == 1) {
            $user_id = $res->fetch(PDO::FETCH_ASSOC)['user'];
            return $user_id;
        }
        return 0;
    }

    function getDisplayName()
    {
        global $user_dp_name, $db;
        if (isset($user_dp_name))
            return $user_dp_name;

        $res = $db->query("SELECT `displayname` FROM `users` WHERE `id` = '" . $this->getUserID() . "';");
        if ($res->rowCount() == 1) {
            $user_dp_name = $res->fetch(PDO::FETCH_ASSOC)['displayname'];
            return $user_dp_name;
        }
        return "no_username";
    }

    function getGroup()
    {
        global $user_group, $db;
        if (isset($user_group))
            return $user_group;

        $res = $db->query("SELECT `group` FROM `users` WHERE `id` = '" . $this->getUserID() . "';");
        if ($res->rowCount() == 1) {
            $user_group = $res->fetch(PDO::FETCH_ASSOC)['group'];
            return $user_group;
        }
        return "no_username";
    }

    function getGravatarURL($size = 100)
    {
        global $debug;
        if ($debug)
            return "";
        return 'https://www.gravatar.com/avatar/' . md5(strtolower(trim($this->getEmail()))) . "?d=identicon&s=" . $size;
    }

    function getEmail()
    {
        global $user_email, $db;
        if (isset($user_email))
            return $user_email;

        $res = $db->query("SELECT `email` FROM `users` WHERE `id` = '" . $this->getUserID() . "';");
        if ($res->rowCount() == 1) {
            $user_email = $res->fetch(PDO::FETCH_ASSOC)['email'];
            return $user_email;
        }
        return "no_username";
    }

    function getGroupID()
    {
        global $user_group, $db;
        if (isset($user_group))
            return $user_group;

        $res = $db->query("SELECT `group` FROM `users` WHERE `id` = '" . $this->getUserID() . "';");
        if ($res->rowCount() == 1) {
            $user_group = $res->fetch(PDO::FETCH_ASSOC)['group'];
            return $user_group;
        }
        return 0;

    }
}

function getSessionKey()
{
    global $session_key;
    if (isset($session_key))
        return $session_key;
    if (isset($_COOKIE['sessionkey'])) {
        $session_key = htmlspecialchars($_COOKIE['sessionkey']);
        return $session_key;
    }
    return "no_key";
}