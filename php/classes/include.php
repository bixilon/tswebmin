<?php
//check if config file exists
if (file_exists($rd . "php/config/config.inc.php"))
    require_once $rd . "php/config/config.inc.php";
else {
    header("Location: /install");
    die("config.inc.php does not exists. Please reinstall");
}

require_once $rd . "php/classes/User.php";
$user = new User();

//lang (de_DE is the only one for now)
require_once $rd . "php/lang/" . $lang . ".php";