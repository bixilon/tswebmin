<?php


//close main
echo '</main>';

//footer

//Please do not remove this hint, support me
echo '<footer class="container">
      <p>' . $language['copyright'] . '</p>
    </footer>';

echo '<script src="/lib/js/popper.min.js"></script>
    <script src="/lib/js/bootstrap/bootstrap.min.js"></script>';
if (isset($offcanavas) && $offcanavas) {
    echo '<script src="/lib/js/holder.min.js"></script>
          <script src="/lib/js/offcanvas.js"></script>';
}
if (isset($tsAdmin) && $debug) {
    if (count($tsAdmin->getDebugLog()) > 0) {
        foreach ($tsAdmin->getDebugLog() as $logEntry) {
            echo '<script>alert("' . $logEntry . '");</script>';
        }
    }
}
echo '</body></html>';