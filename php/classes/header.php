<?php

if (!isset($title))
    $title = "Unbekannt";

//imports


echo '<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">
    <title>' . $title . ' - ' . $name . '</title>
    <script src="/lib/js/jquery-3.4.1.min.js"></script>';

//req login css
if (isset($css_login) && $css_login)
    echo '<link href="lib/css/floating-labels.css" rel="stylesheet">
          <link href="/lib/css/bootstrap-4/bootstrap.min.css" rel="stylesheet">
          <link href="/lib/css/login.css" rel="stylesheet">';
else
    echo '
    <!-- Bootstrap core CSS -->
    <link href="/lib/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/lib/css/jumbotron.css" rel="stylesheet">';

if (isset($offcanavas) && $offcanavas) {
    echo '<link href="/lib/css/offcanvas.css" rel="stylesheet">';

}

echo '</head>';

//if this is true, nothing will get displayed here
if (!isset($custom) || !$custom) {
//navbar
    echo '<body>

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
      <a class="navbar-brand" href="#">' . $name . '</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/">' . $language['title_dashboard'] . '</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $language['dashboard_client'] . '</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="/clients.php">' . $language['overview'] . '</a>
              <a class="dropdown-item" href="/channel-browser.php">' . $language['dashboard_client_channel_browser'] . '</a>
              <a class="dropdown-item" href="/tokens.php">' . $language['dashboard_client_tokens'] . '</a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown01" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' . $language['dashboard_channels'] . '</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
              <a class="dropdown-item" href="/channels.php">' . $language['overview'] . '</a>
            </div>
          </li>
        </ul>
        <form action="/logout.php" class="form-inline my-2 my-lg-0">
          <button class="btn btn-outline-danger my-2 my-sm-0" type="submit">' . $language['logout'] . '</button>
        </form>
      </div>
    </nav>
    <noscript><div class="alert alert-warning m-3">' . $language['warn_no_js'] . '</div></noscript>';

    echo '<main role="main" class="m-3">';
}