<?php

use par0noid\ts3admin;

require_once $rd . "php/lib/ts3admin.class.php";

$tsAdmin = new ts3admin($ts_host, $ts_queryport);


if ($tsAdmin->getElement('success', $tsAdmin->connect())) {
    $tsAdmin->login($ts_username, $ts_password);
    $tsAdmin->selectServer($ts_port);

} else {
    echo("Connection could not be established.");
    if (count($tsAdmin->getDebugLog()) > 0) {
        foreach ($tsAdmin->getDebugLog() as $logEntry) {
            echo '<script>alert("' . $logEntry . '");</script>';
        }
    }
    die();
}