<?php

$language['title_dashboard'] = "Dashboard";
$language['title_clients_and_channels'] = "Clients und Channel";
$language['title_channels'] = "Channel";
$language['title_tokens'] = "Berechtigungsschlüssel";


$language['login_header'] = "Bitte Anmelden!";
$language['placeholder_username'] = "Benutzername";
$language['placeholder_password'] = "Passwort";
$language['placeholder_2fa'] = "2FA Anmelde Code";
$language['login_remember_me'] = "Angemeldet bleiben";
$language['login_sign_in'] = "Anmelden";

$language['login_error_field_not_set'] = "Alle Felder müssen ausgefüllt werden";
$language['login_wrong'] = "Benutzername, Passwort oder 2FA Code falsch!";


$language['logout'] = "Abmelden";
$language['continue'] = "Weiter &raquo;";
$language['overview'] = "Übersicht";
$language['cancel'] = "Abbrechen";
$language['dropdown_please_select'] = "Bitte wählen";
$language['warn_no_js'] = "Diese Seite braucht JavaScript um richtig zu funktionieren. Manche Sachen werden eventuell nicht verfügbar sein.";

$language['dashboard_hello'] = "Hallo, ";
$language['dashboard_hello_sub'] = "Willkommen im TeamSpeak 3/5 Webinterface!";
$language['dashboard_hello_learn_more'] = "Mehr erfahren &raquo;";


$language['dashboard_channels'] = "Channels";
$language['dashboard_channels_sub'] = "Verwalte alle Channels von deinem TeamSpeak Server.";

$language['dashboard_client'] = "Clients";
$language['dashboard_client_tokens'] = "Berechtigungsschlüssel";
$language['dashboard_client_sub'] = "Verwalte Clients, gehe Beschwerden nach.";

$language['clients_token_no_tokens'] = "Es wurden noch keine Tokens erstellt";
$language['clients_token_server_group'] = "Server";
$language['clients_token_channel_group'] = "Channel";

$language['clients_token_table_token'] = "Token";
$language['clients_token_table_type'] = "Typ";
$language['clients_token_table_group'] = "Gruppe";
$language['clients_token_table_channel'] = "Channel";
$language['clients_token_table_created'] = "Erstellt";
$language['clients_token_table_description'] = "Beschreibung";
$language['clients_token_table_action'] = "Action";
$language['clients_token_table_delete'] = "Löschen";

$language['clients_token_add'] = "Hinzufügen";
$language['clients_token_add_modal_action'] = "Hinzufügen";
$language['clients_token_add_modal_close'] = "Abbrechen";
$language['clients_token_add_modal_title'] = "Berechtigungsschlüssel hinzufügen";
$language['clients_token_add_modal_form_servergroup'] = "Servergruppe";
$language['clients_token_add_modal_form_channelgroup'] = "Channelgruppe";
$language['clients_token_add_modal_form_placeholder_group'] = "Gruppe";
$language['clients_token_add_modal_form_placeholder_channel'] = "Channel";
$language['clients_token_add_modal_form_placeholder_description'] = "Beschreibung";
$language['clients_token_add_successfully'] = "Token <strong>%0</strong > wurde erstellt";

$language['dashboard_client_channel_browser'] = "Channel Browser";
$language['client_channel_status_deaf'] = "Taub";
$language['client_channel_status_muted'] = "Stumm";

$language['channel_status_ful'] = "Voll";
$language['channel_status_password_protected'] = "Passwort geschützt";
$language['channel_status_no_talk_power'] = "Talkpower benötigt";
$language['channel_action_edit'] = "Bearbeiten";

$language['client_channel_modal_button_kick'] = "Kicken";
$language['client_channel_modal_form_reason_label'] = "Grund";
$language['client_channel_modal_kick_title'] = "Client kicken";
$language['client_channel_modal_kick_type_server'] = "Serverkick";
$language['client_channel_modal_kick_type_channel'] = "Channelkick";
$language['client_kicked_successfully'] = "Client erfolgreich gekickt!";


$language['client_channel_modal_button_poke'] = "Anstupsen";
$language['client_channel_modal_poke_title'] = "Client anstupsen!";
$language['client_channel_modal_form_message'] = "Nachricht";
$language['client_poked_successfully'] = "Client erfolgreich angestupst!";
$language['client_action_kick'] = "Kicken";
$language['client_action_poke'] = "Anstupsen";
$language['client_action_info'] = "Info";
$language['client_info_client_not_online'] = "<b>Fehler:</b> Client ist nicht (mehr) online!<br>Versuchst über den <a href=\"clients.php\">Offline Client Browser</a>!";


$language['copyright'] = " &copy; Bixilon 2020";